# How to run

1. run `bundle install` in order to install dependencies
2. run `bundle exec guard` or `bundle exec rspec` to be sure the module is working

See usage examples in rspec tests.
