# frozen_string_literal: true

# Custom validation module
# Usage:
# - put "require 'lib/validation.rb'" to your class
# - put "include Validation" to your class
module Validation
  VALID_OPTIONS = %i[presence format type].freeze

  NoSuchAttribute = Class.new(StandardError)
  NoSuchValidator = Class.new(StandardError)
  Failure         = Class.new(StandardError)

  # Base class
  class Validator
    def initialize(value, with)
      @value = value
      @with = with
    end

    def valid?
      raise NotImplementedError
    end
  end

  # check if present
  class PresenceValidator < Validator
    def valid?
      return true if @with != true

      @value.to_s != ''
    end
  end

  # regexp format
  class FormatValidator < Validator
    def valid?
      (@value =~ @with).nil? == false
    end
  end

  # Ruby Type check
  class TypeValidator < Validator
    def valid?
      @value.is_a?(@with)
    end
  end

  # self descriptive
  module ClassMethods
    ValidationHolder = Struct.new(:name, :options)

    def validate(field, opts)
      validator = opts.keys.first
      begin
        validator_klass(validator)
      rescue NameError
        raise NoSuchValidator, validator
      end

      init_validates

      new_validation = ValidationHolder.new(field, opts)
      class_variable_set(:@@validates, existing_validates << new_validation)
    end

    def validator_klass(validator_symbol)
      const_get("Validation::#{validator_symbol.capitalize}Validator")
    end

    private

    def existing_validates
      class_variable_get(:@@validates)
    end

    def init_validates
      return if class_variable_defined?(:@@validates)

      reset_validations
    end

    def reset_validations
      class_variable_set(:@@validates, [])
    end
  end
  # ClassMethods

  def self.included(klass)
    klass.extend(ClassMethods)
  end

  # rubocop:disable Metrics/AbcSize
  # runs all checks and validations
  def validate!
    check_fields

    validates.each do |v|
      validator = v.options.keys.first
      validator_klass = self.class.validator_klass(validator)
      value = public_send(v.name)

      raise Failure, "#{validator_klass} error" unless validator_klass.new(
        value, v.options[validator]
      ).valid?
    end
  end
  # rubocop:enable Metrics/AbcSize

  # returns boolean value of validation result
  def valid?
    validate!
    true
  rescue Validation::Failure
    false
  end

  private

  def check_fields
    validates.each do |v|
      raise NoSuchAttribute, v.name unless respond_to?(v.name)
    end
  end

  def validates
    self.class.class_variable_get(:@@validates)
  end
end
