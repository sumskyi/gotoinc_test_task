#!ruby
# frozen_string_literal: true

require 'pry'
require './lib/validation.rb'

class User; end

# just
class Admin
  attr_accessor :name, :number, :owner
  include Validation

  def initialize(name, number, owner)
    @name = name
    @number = number
    @owner = owner
  end

  validate :name, presence: true
end

admin = Admin.new('Vasya', '2345', User.new)

valid = admin.valid?
p "admin.valid? =>"
p valid
