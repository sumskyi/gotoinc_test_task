# frozen_string_literal: true

require 'spec_helper'
require 'pry'

class Model
  attr_accessor :name, :number, :owner, :just_test
  include Validation
end

RSpec.describe Validation do
  describe 'methods presence' do
    context 'class methods' do
      it '.validate' do
        expect(subject::ClassMethods.instance_methods).to include(:validate)
      end
    end

    context 'instance methods' do
      let(:instance_methods) { subject.instance_methods }

      it '#validate!' do
        expect(instance_methods).to include(:validate!)
      end

      it '#valid?' do
        expect(instance_methods).to include(:valid?)
      end
    end
  end
end

describe Model do
  subject { Model.new }

  context 'check atrributes' do
    context 'valid' do
      before do
        Model.send(:reset_validations)

        Model.class_eval do
          validate :name, presence: true
        end
      end

      it 'ok' do
        subject.name = 'wow'
        expect(subject).to be_valid
      end

      it 'invalid' do
        expect(subject).to_not be_valid
      end
    end
  end
end

describe Validation::PresenceValidator do
  it 'works with smthng' do
    expect(Validation::PresenceValidator.new('asdf', true)).to be_valid
  end

  it 'works with nil' do
    expect(Validation::PresenceValidator.new(nil, true)).to_not be_valid
  end

  it 'works with empty string' do
    expect(
      Validation::PresenceValidator.new('', true)
    ).to_not be_valid
  end
end

describe Validation::FormatValidator do
  let(:email_regexp) { /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i }
  let(:valid_input) { 'sumskyi@gmail.com' }
  let(:invalid_input) { 'somewhere' }

  it 'valid' do
    expect(
      Validation::FormatValidator.new(valid_input, email_regexp)
    ).to be_valid
  end

  it 'invalid' do
    expect(
      Validation::FormatValidator.new(invalid_input, email_regexp)
    ).to_not be_valid
  end
end

describe Validation::TypeValidator do
  let(:valid_input) { User.new }
  let(:invalid_input) { Admin.new }

  class User; end
  class Admin; end

  it 'valid' do
    expect(
      Validation::TypeValidator.new(valid_input, User)
    ).to be_valid
  end

  it 'invalid' do
    expect(
      Validation::TypeValidator.new(invalid_input, User)
    ).to_not be_valid
  end
end
